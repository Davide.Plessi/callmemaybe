var express = require('express');
var bodyParser = require('body-parser');

var app = express();
var port = process.env.PORT || 3000;

app.use(bodyParser.text());
app.use(bodyParser.urlencoded({extended: true}));

app.post('/', function(req, res) {
    console.log(req);
    console.log(req.body);
    console.log(req.headers);
    
    res.sendStatus(404)
});

app.listen(port);